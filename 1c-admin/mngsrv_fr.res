  �'      ResB             *     C  +  +  �       �  IDS_SETPROP_SESSIONPARAMSETTINGS_ONLY IDS_EDITBTNTOOLTIP_CLEAR IDS_EDITBTNTOOLTIP_LISTSEL IDS_EDITBTNTOOLTIP_SPECSEL IDS_EDITBTNTOOLTIP_OPEN IDS_EDITBTNTOOLTIP_UP IDS_EDITBTNTOOLTIP_DOWN IDS_EDITBTNTOOLTIP_DROPLIST IDS_ADDIN_TITLE IDS_FSE_TITLE IDS_CPE_TITLE IDS_AGENT_TITLE IDS_CRYPTOEXTENSION IDS_HTML_COPYRIGHT IDS_HTML_RUN_MODE_DESCR IDS_HTML_EXTERN_MODULE_INSTALL_IN_PROGRESS IDS_HTML_EXTERN_MODULE_INSTALLED IDS_HTML_EXTERN_MODULE_ALREADY_INSTALLED IDS_HTML_EXTERN_MODULE_INSTALL_FAILED IDS_CONFIRM_INSTALL_IE IDS_VERIFY_PUBLISHER IDS_INSTALL_NOW_IE IDS_PRESS_CONTINUE IDS_FILE_EXT_INSTALL IDS_CRYPTO_EXT_INSTALL IDS_AGENT_EXT_INSTALL IDS_EXT_MODULE_INSTALL IDS_CHROME_EXT_INSTALL_AGREE_MSG IDS_CHROME_EXT_INSTALL_REJECT_MSG IDS_SAFARI_EXT_INSTALL_TIP_MACOS IDS_SAFARI_EXT_INSTALL_DOWNLOAD IDS_CHROME_37_EXT_INSTALL_BEGIN IDS_CHROME_37_EXT_INSTALL_FILES_EXT IDS_CHROME_37_EXT_INSTALL_CRYPTO_EXT IDS_CHROME_37_EXT_INSTALL_AGENT_EXT IDS_CHROME_37_EXT_INSTALL_ADDIN_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_CHROME_37_EXT_INSTALL_NEED_RESTART IDS_CHROME_37_EXT_INSTALL_WAIT IDS_CHROME_37_EXT_INSTALL_WAIT_LIN IDS_CHROME_37_EXT_INSTALL_RESTART IDS_EDGE_START_IN_IE11 IDS_CHROME_CLIPBOARD_EXT_NOT_INSTALLED IDS_CHROME_CLIPBOARD_EXT_NEED_RESTART IDS_CHROME_EXT_ATTEMPT_TO_USE IDS_CHROME_EXT_INSTALL_MAYBE_LATER IDS_FF_EXT_INSTALL_WEB_EXT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_FF_CLIPBOARD_EXT_NOT_INSTALLED IDS_HTML_BROWSER_SETTINGS_INFO_CONFIRMATION IDS_HTML_COLOR_CHOOSE IDS_HTML_RED IDS_HTML_GREEN IDS_HTML_BLUE IDS_HTML_BACKGOUND_COLOR_PREVIEW IDS_HTML_TEXT_COLOR_PREVIEW IDS_HTML_OPEN_CALCULATOR IDS_HTML_OPEN_CALENDAR IDS_HTML_COPY_TO_CLIPBOARD_AS_NUMBER IDS_HTML_ADD_NUMBER_TO_CLIPBOARD IDS_HTML_SUBTRACT_NUMBER_FROM_CLIPBOARD IDS_HTML_SERVICE IDS_HTML_FORM_CUSTOMIZATION IDS_HTML_TOOLBAR_CUSTOMIZE IDS_HTML_ADD_REMOVE_BUTTONS IDS_HTML_ADD_GROUP IDS_HTML_ADD_FIELDS IDS_HTML_DELETE_CURRENT_ITEM IDS_HTML_MOVE_CURRENT_ITEM_UP IDS_HTML_MOVE_CURRENT_ITEM_DOWN IDS_HTML_MARK_ALL IDS_HTML_UNMARK_ALL IDS_HTML_DELETE IDS_HTML_MOVE_UP IDS_HTML_MOVE_DOWN IDS_HTML_RESTORE_DEFAULT_SETTINGS IDS_HTML_FORM_ITEM_PROPERTIES IDS_HTML_CHOOSE_FIELDS_TO_PLACE_ON_FORM IDS_HTML_LOADING IDS_HTML_THEMES_PANEL IDS_HTML_NAVIGATION_PANEL IDS_HTML_NAVIGATION2_PANEL IDS_HTML_OPEN_HELP IDS_HTML_SHOW_ABOUT IDS_HTML_FAVORITE_LINKS IDS_HTML_FAVORITES IDS_HTML_HISTORY IDS_HTML_ENTER_NAME_AND_PASSWORD IDS_HTML_EXIT IDS_HTML_REPEAT_ENTER IDS_HTML_FILE_UPLOADING IDS_HTML_FILE IDS_HTML_UPLOADING IDS_HTML_APPLY IDS_HTML_SHOW_BUTTON IDS_HTML_HISTORY_TITLE IDS_HTML_TOPICSELECTOR_TITLE IDS_HTML_FILE_DOWNLOADING IDS_RTE_ACTIVEDOCUMENTGET_ERROR IDS_RTE_CONNECTION_UNEXPECTEDLY_CLOSED IDS_RESET_TOOLBAR IDS_HTML_CLIPSTORE IDS_HTML_CLIPPLUS IDS_HTML_CLIPMINUS IDS_HTML_WINDOW_ACTIVATED_MSG IDS_HTML_UNABLE_TO_SWITCH_TO_MAINWINDOW IDS_HTML_CLIPBOARD_INACCESSIBLE IDS_HTML_USE_BROWSER_CLIPBOARD_COMMANDS IDS_HTML_CLIPBOARD_OPERATION_DISABLED IDS_TE_TITLE IDS_TE_MESSAGE IDS_TE_INTERRUPT IDS_CMD_HELPTRAINING IDS_WEB_COMPAT_PLATFORM_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_VERSION_UNSUPPORTED IDS_WEB_COMPAT_WORK_IMPOSSIBLE IDS_WEB_BROWSERSLIST_HEADER IDS_WEB_BROWSERSLIST_DOWNLOAD IDS_WEB_BROWSERSLIST_VERSION_IE IDS_WEB_BROWSERSLIST_VERSION_FF IDS_WEB_BROWSERSLIST_VERSION_CHROME IDS_WEB_BROWSERSLIST_VERSION_SAFARI IDS_WEB_BROWSERSLIST_VERSION_SAFARI_IPAD IDS_WEB_BROWSER_UPDATE_LINK_IE IDS_WEB_BROWSER_UPDATE_LINK_SAFARI IDS_WEB_BROWSER_UPDATE_LINK_SAFARI_IPAD IDS_HTML_SET_WARN_EXT_COMP IDS_HTML_SET_GET_FILE IDS_HTML_SET_WARN_LINK_GET_FILE IDS_EXECUTE_NOT_SUPPORTED IDS_SEARCH_PROCESSING IDS_LOGOUT_WORK_FINISH IDS_WORK_IN_FULLSCREEN IDS_WORK_ONLY_IN_FULLSCREEN IDS_START_WORKING IDS_CONTINUE_WORK IDS_ECS_BROWSER_NOT_SUPPORTED IDS_ECS_MEDIA_REQUIRED_SECURE_ORIGIN IDS_ECS_MEDIA_NOT_AVAILABLE IDC_OIDC_STANDARD_LOGIN_NAME IDC_OIDC_ANOTHER_SERVICES IDC_OIDC_ERROR browsersettingsinfoieru.html browsersettingsinfoieen.html browsersettingsinfochru.html browsersettingsinfochen.html browsersettingsinfosfru.html browsersettingsinfosfen.html browsersettingsinfoff.html browsersettingsinfomain.html   B l e u   V e r t   R o u g e   E n t r e r   E r r e u r   D � d u i r e   F a v o r i s   A j o u t e r   L i c e n c e   S e r v i c e   F i c h i e r   r � d u i r e   A f f i c h e r   S u p p r i m e r   C o n t i n u e r   A p p l i q u e r   a g g r a n d i r   C h a r g e m e n t   H i s t o r i q u e   A b a n d o n n e r   T � l � c h a r g e r   R e d � m a r r e r ?   R e c h e r c h e . . .   M a r q u e r   t o u s   O u v r i r   l ' a i d e   L i e n s   f a v o r i s   C h a r g e m e n t . . .   A u t r e s   b o u t o n s   P a g e s   v i s i t � e s   B a r r e   d  a c t i o n   E x e m p l e   d e   f o n d   �   l a   p r o c h a i n e !   E x e m p l e   d e   t e x t e   s � l e c t i o n n e r ( F 4 )   A j o u t e r   u n   g r o u p e   c o m p o s a n t   e x t e r n e   V e r s i o n   � d u c a t i v e   A j o u t e r   l e s   c h a m p s   E f f a c e r   ( S h i f t + F 4 )   C o m m e n c e r l e t r a v a i l   C o n n e c t e r   �   n o u v e a u   S e c t i o n s   d u   p a n n e a u   C o p i e r   c o m m e   n o m b r e   T e r m i n e r   l e   t r a v a i l   B a r r e   d e   n a v i g a t i o n   R � c e p t i o n   d u   f i c h i e r   O u v r i r   l e   c a l e n d r i e r   D � p l a c e r   v e r s   l e   b a s   E x � c u t i o n   d e s   t � c h e s   O u v r i r ( C t r l + S h i f t + F 4 )   S � l e c t i o n   d u   c h a p i t r e   R e d � m a r r e r   m a i n t e n a n t   R � g l a g e   d e   l  a t t r i b u t   C h a r g e m e n t   d u   f i c h i e r   " I n s t a l l e r   /   I n s t a l l "   D � p l a c e r   v e r s   l e   h a u t   O u v r i r   l a   c a l c u l a t r i c e   D � m a r r e r   l ' i n s t a l l a t i o n   R e p o r t e r   l ' i n s t a l l a t i o n   S � l e c t i o n   d e   l a   c o u l e u r   T � l � c h a r g e r   l e   c o m p o s a n t   D � c o c h e r   t o u s   l e   � l � m e n t s   C a m � r a   v i d � o   i n d i s p o n i b l e   S u p p r i m e r   l  � l � m e n t   a c t u e l   T r a v a i l   e n   m o d e   p l e i n   � c r a n   R � i n i t i a l i s a t i o n   d u   p a n n e a u   �   T R A V E R S   D ' A U T R E S   S E R V I C E S   h t t p : / / w w w . a p p l e . c o m / r u / i o s /   P e r s o n n a l i s a t i o n   d e   f o r m u l a i r e   L ' e x p a n s i o n   d e   l a   c r y p t o g r a p h i e   L i s t e   d e s   n a v i g a t e u r s   s u p p o r t � s   A j o u t e r   o u   s u p p r i m e r   d e s   b o u t o n s   D � f i n i r   l e s   r � g l a g e s   s t a n d a r d . . .   L  o p � r a t i o n   n  e s t   p a s   d i s p o n i b l e .   S � l e c t i o n n e r   �   p a r t i r   d e   l a   l i s t e   A j o u t e r   u n   n o m b r e   a u   p r e s s e - p a p i e r s   E n l e v e r   u n   n o m b r e   d u   p r e s s e - p a p i e r s   e x t e n s i o n   d u   t r a i t e m e n t   d e s   f i c h i e r s   D � p l a c e r   l  � l � m e n t   a c t u e l   v e r s   l e   b a s   P r o p r i � t � s   d e   l ' � l � m e n t   d e   f o r m u l a i r e   I n s t a l l a t i o n   d e   l a   c o m p o s a n t e   e x t e r n e .   D � p l a c e r   l  � l � m e n t   a c t u e l   v e r s   l e   h a u t   A f f i c h e r   l  i n f o r m a t i o n   s u r   l e   p r o g r a m m e   I m p o s s i b l e   d e   s u p p r i m e r   u n   i n d e x   u n i q u e   h t t p : / / w w w . a p p l e . c o m / r u / s a f a r i / d o w n l o a d /   �   1 C : E n t e r p r i s e   -   A l e r t e s   e t   d � m a r r a g e � �   *�C o p i e r   d a n s   l e   p r e s s e - p a p i e r s   c o m m e   n o m b r e   *�C o m m e n t   p u i s - j e   c o n f i g u r e r   m o n   n a v i g a t e u r ?   +�e x t e n s i o n   d u   t r a i t e m e n t   d e   l a   c r y p t o g r a p h i e   -�S � l e c t i o n n e r   �   p a r t i r   d e   l a   l i s t e   ( C t r l + D o w n )   /�T r a v a i l   u n i q u e m e n t   p o s s i b l e   e n   m o d e   p l e i n   � c r a n   0�L e   c o m p o s a n t   e x t e r n e   a   � t �   i n s t a l l �   a v e c   s u c c � s .   1ܩ     " 1 !- S o f t " ,   1 9 9 6 - 2 0 1 8 .   T o u s   d r o i t s   r � s e r v � s .   2�P a s s e   �   l a   f e n � t r e .   C l i q u e z   s u r   O K   p o u r   c o n t i n u e r .   4�L e   c o m p o s a n t   e x t e r n e   e s t   e n   c o u r s   d ' i n s t a l l a t i o n   . . .   4�S i   v o u s   n e   p o u v e z   p a s   o b t e n i r   l e   f i c h i e r ,   c o n f i g u r e r   5�S � l e c t i o n n e z   l e   c h a m p   p o u r   l  e m p l a c e m e n t   s u r   l a   f o r m e   7�P o u r   c o m m e n c e r   l ' i n s t a l l a t i o n ,   c l i q u e z   s u r   " C o n t i n u e r " .   9�I n s t a l l a t i o n   d e   l  e x t e n s i o n   d u   t r a v a i l   a v e c   l e s   f i c h i e r s .   :�L  o p � r a t e u r   E x � c u t e r   d a n s   l e   c l i e n t   W e b   n  e s t   p a s   s u p p o r t �   :�I n s t a l l a t i o n   d e   l ' e x t e n s i o n   t r a v a i l l e r   �   l a   c r y p t o g r a p h i e .   =�L e   c o m p o s a n t   e x t e r n e   a   � t �   o u   a v a i t   � t �   i n s t a l l �   a v e c   s u c c � s .   >�V o t r e   n a v i g a t e u r   n e   p e r m e t   p a s   l ' a c c � s   d a n s   l e   p r e s s e - p a p i e r s .   @�L e s   t r a v a u x   d ' a r r i � r e - p l a n   s o n t   e n   c o u r s . 
 V e u i l l e z   p a t i e n t e r   . . .   A�L e   n a v i g a t e u r   u t i l i s �   n e   s u p p o r t e   p a s   l e   t r a v a i l   a v e c   1 !: D i a l o g u e   C�I l   y   a v a i t   u n   p e t i t   p r o b l � m e   t e m p o r a i r e   a v e c   l a   c o n n e x i o n   I n t e r n e t .   C�I n s t a l l a t i o n   d u   p r o g r a m m e   �   1 C : E n t e r p r i s e   -   A l e r t e s   e t   d � m a r r a g e � � .   E�I n s t a l l a t i o n   d e   l  e x t e n s i o n   d u   t r a v a i l   a v e c   l e s   f i c h i e r s   e s t   e x � c u t � e   E�S a i s i s s e z   l e   n o m   e t   l e   m o t   d e   p a s s e   d e   l  u t i l i s a t e u r   d e   1 C : E n t r e p r i s e   E�L a   t r a n s m i s s i o n   v e r s   l a   f e n � t r e   p r i n c i p a l e   n e   p e u t   p a s   � t r e   e x � c u t � e .   E�h t t p : / / w i n d o w s . m i c r o s o f t . c o m / r u - R U / i n t e r n e t - e x p l o r e r / p r o d u c t s / i e / h o m e   G�P o u r   a c c � d e r   �   l a   c a m � r a   v i d � o ,   i l   f a u t   u n e   c o n n e x i o n   s � c u r i s � e   ( h t t p s )   O�V e r s i o n s   s u p p o r t � e s   :   1 0   e t � s u p � r i e u r e s . < b r / > V e r s i o n   r e c o m m a n d � e   :   l a   d e r n i � r e .   O�I n s t a l l a t i o n   d u   p r o g r a m m e   �   1 C : E n t e r p r i s e   -   A l e r t e s   e t   d � m a r r a g e � �   e s t   e n   c o u r s   O�V e r s i o n s   s u p p o r t � e s   :   3 8   e t   s u p � r i e u r e s . < b r / > V e r s i o n   r e c o m m a n d � e   :   l a   d e r n i � r e .   P�V e r s i o n s   s u p p o r t � e s   :   4 . 0   e t   s u p � r i e u r e s . < b r / > V e r s i o n   r e c o m m a n d � e   :   l a   d e r n i � r e .   Q�C O M - o b j e t s   s o n t   p r i s   e n   c h a r g e   u n i q u e m e n t   s u r   l e s   s y s t � m e s   d ' e x p l o i t a t i o n   W i n d o w s   R�V e r s i o n s   s u p p o r t � e s   :   4 . 0 . 5   e t � s u p � r i e u r e s . < b r / > V e r s i o n   r e c o m m a n d � e   :   l a   d e r n i � r e .   T�< p > P o u r   i n s t a l l e r   % s ,   r e n d r e   l e   f i c h i e r   o b t e n u   c o m m e   e x � c u t a b l e ,   p u i s   l a n c e z - l e . < / p >   X�V o u s   p o u v e z   u t i l i s e r   C t r l   +   C   p o u r   c o p i e r ,   C t r l   +   X   p o u r   c o u p e r   e t   C t r l   +   V   p o u r   c o l l e r .   Y�P o u r   a n n u l e r   l  i n s t a l l a t i o n ,   c l i q u e r   s u r   " A n n u l e r   l e   t r a n s f e r t / D i s c a r d "   870:@>9B5MB>>:=>.   \�L e   c o m p o s a n t   e x t e r n e   n ' a   p a s   � t �   i n s t a l l �   ! 
 U n e   e r r e u r   e s t   s u r v e n u e   l o r s   d e   l ' i n s t a l l a t i o n   !   \�V e r s i o n s   s u p p o r t � e s   :   4 . 0 . 4   ( i O S   3 . 2 )   e t   s u p � r i e u r e s . < b r / > V e r s i o n   r e c o m m a n d � e   :   l a   d e r n i � r e .   ^�< p > P o u r   i n s t a l l e r   % s ,   c l i q u e r   s u r   l e   f i c h i e r   r e � u ,   p u i s   a t t e n d r e   l a   f i n   d e   l  i n s t a l l a t i o n . < / p >   ^�L a   p r o p r i � t �   n e   p e u t   p a s   � t r e   m o d i f i � e   a p r � s   a v o i r   a p p e l �   l a   f o n c t i o n   D � f i n i r P a r a m � t r e s S e s s i o n   b�L o r s q u e   v o u s   � t e s   i n v i t �   �   a u t o r i s e r   l ' i n s t a l l a t i o n ,   c l i q u e z   s u r   l e   b o u t o n   "   I n s t a l l e r   /   I n s t a l l " .   ~�P o u r   l a n c e r   l  i n s t a l l a t i o n ,   c l i q u e r   s u r   " S u i v a n t / C o n t i n u e " .   P u i s ,   a c c e p t e r   l  i n s t a l l a t i o n   e n   c l i q u a n t   s u r   " I n s t a l l e r / I n s t a l l " .   ��A p r � s   a v o i r   c o r r e c t e m e n t   i n s t a l l �   l ' e x t e n s i o n   p o u r   l e   n a v i g a t e u r ,   r e c h a r g e z   l a   p a g e .   
 C l i q u e z   s u r   " R e c h a r g e r   m a i n t e n a n t "   p o u r   l e   r e c h a r g e m e n t .   ��L e   s i t e   % h o s t n a m e %   e s s a i e   d e   f a i r e   a p p e l   a u   p r e s s e - p a p i e r s .   A p p u y e z   s u r   ' O K '   p o u r   a u t o r i s e r   c e t   a c c � s .   C l i q u e z   s u r   A n n u l e r   p o u r   r e f u s e r   c e t   a c c � s .   ��L a   v e r s i o n   d u   n a v i g a t e u r   q u e   v o u s   u t i l i s e z   n ' e s t   p a s   i n c l u s e   d a n s   l a   l i s t e   1 C : E n t e r p r i s e . 
   C l i q u e z   s u r   O K   p o u r   a c c � d e r   �   l a   l i s t e   d e s   n a v i g a t e u r s   p r i s   e n   c h a r g e .   ��< p > A p r � s   a v o i r   c o r r e c t e m e n t   i n s t a l l �   l ' e x t e n s i o n   p o u r   l e   n a v i g a t e u r   e t   l e   c o m p o s a n t ,   r e c h a r g e z   l a   p a g e .   C l i q u e z   s u r   < b > " R e d � m a r r e r   m a i n t e n a n t " < / b >   p o u r   r e d � m a r r e r . < / p >   ��< p > L ' e x t e n s i o n   a   � t �   i n s t a l l � e .   L ' i n s t a l l a t i o n   d e   % s   s e r a   m a i n t e n a n t   e f f e c t u � e . < / p > < p > C h a n g e z   l ' e x t e n s i o n   d u   f i c h i e r   p o u r   q u ' i l   s o i t   e x � c u t a b l e   e t   d � m a r r e r   l ' i n s t a l l a t i o n . < / p >   ��< p > L ' e x t e n s i o n   a   � t �   i n s t a l l � e .   I l   r e s t e   �   i n s t a l l e r   % s . < / p > < p > P o u r   c e   f a i r e ,   c l i q u e z   s u r   l e   f i c h i e r   ( e n   b a s   d e   l a   f e n � t r e   d u   n a v i g a t e u r )   e t   a t t e n d e z   j u s q u ' �   l a   f i n   d e   l ' i n s t a l l a t i o n . < / p > < p >   ��L o r s q u e   v o u s   � t e s   i n v i t �   p o u r   l ' i n s t a l l a t i o n ,   a s s u r e z - v o u s   d e   v o u s   c o n n a i s s e z   l ' a u t e u r   e s t   l e   f o u r n i s s e u r   d e   c o m p o s a n t !   P o u r   a n n u l e r   l ' i n s t a l l a t i o n ,   c l i q u e z   s u r   " A n n u l e r   /   C a n c e l " .   P o u r   c o n f i r m e r   l  i n s t a l l a t i o n ,   c l i q u e z   s u r   ��P o u r   a c c � d e r   �   c e t t e   f o n c t i o n n a l i t � ,   i l   e s t   r e c o m m a n d �   d e   l a n c e r   l  a p p l i c a t i o n   d a n s   l e   n a v i g a t e u r   I n t e r n e t   E x p l o r e r . 
 P o u r   c e l a ,   o u v r i r   l e   m e n u   d u   n a v i g a t e u r   �   l  a i d e   d u   b o u t o n   " . . . " ,   p u i s   s � l e c t i o n n e r   " O u v r i r   d a n s   I n t e r n e t   E x p l o r e r " .   �L e   n a v i g a t e u r   q u e   v o u s   u t i l i s e z   n ' e s t   p a s   i n c l u s   d a n s   l a   l i s t e   d e s   n a v i g a t e u r s   s u p p o r t � s   p a r   1 C : E n t e r p r i s e . 
 L e   f o n c t i o n n e m e n t   d u   s y s t � m e   p e u t   � t r e   i n c o r r e c t 
 C l i q u e z   s u r   O K   p o u r   c o n t i n u e r . 
 C l i q u e z   s u r   A n n u l e r   ( C a n c e l )   p o u r   a c c � d e r   �   l a   l i s t e   d e s   n a v i g a t e u r s   p r i s   e n   c h a r g e .   �< p > L ' e x t e n s i o n   a   � t �   i n s t a l l � e .   L ' i n s t a l l a t i o n   d e   % s   s e r a   m a i n t e n a n t   e f f e c t u � e . < / p > < p > C l i q u e z   s u r   < b > C o n t i n u e r < / b >   p o u r   d � m a r r e r   l ' i n s t a l l a t i o n . < / p > < p > L e   t � l � c h a r g e m e n t   d u   c o m p o s a n t   s e r a   e f f e c t u � .   E n s u i t e ,   c l i q u e z   s u r   l e   f i c h i e r   e t   a t t e n d e z   j u s q u ' �   l a   f i n   d e   l ' i n s t a l l a t i o n . < / p >   �L a   v e r s i o n   d u   n a v i g a t e u r   q u e   v o u s   u t i l i s e z   n ' e s t   p a s   i n c l u s e   d a n s   l a   l i s t e   d e s   v e r s i o n s   s u p p o r t � e s   p a r   1 C : E n t e r p r i s e . 
 L e   f o n c t i o n n e m e n t   d u   s y s t � m e   p e u t   � t r e   i n c o r r e c t 
 C l i q u e z   s u r   O K   p o u r   c o n t i n u e r . 
 C l i q u e z   s u r   A n n u l e r   ( C a n c e l )   p o u r   a c c � d e r   �   l a   l i s t e   d e s   n a v i g a t e u r s   p r i s   e n   c h a r g e .   �< p > P o u r   e x � c u t e r   l ' a c t i o n   % s   d o i t   � t r e   i n s t a l l � . < / p > < p > C l i q u e z   s u r   < b > C o n t i n u e r < / b >   p o u r   d � m a r r e r   l ' i n s t a l l a t i o n . < / p > < p > L e   t � l � c h a r g e m e n t   d u   c o m p o s a n t   s e r a   e f f e c t u � .   E n s u i t e ,   c l i q u e z   s u r   l e   f i c h i e r   ( e n   b a s   d e   l a   f e n � t r e   d u   n a v i g a t e u r )   e t   a t t e n d e z   j u s q u ' �   l a   f i n   d e   l ' i n s t a l l a t i o n . < / p >   8�L e   s y s t � m e   d ' e x p l o i t a t i o n   q u e   v o u s   u t i l i s e z   n ' e s t   p a s   i n c l u s   d a n s   l a   l i s t e   d e s   s y s t � m e s   s u p p o r t � s   p a r   1 C : E n t e r p r i s e . 
 L e   f o n c t i o n n e m e n t   d u   s y s t � m e   p e u t   � t r e   i n c o r r e c t 
 C l i q u e z   s u r   O K   p o u r   c o n t i n u e r . 
 C l i q u e z   s u r   A n n u l e r   ( C a n c e l )   p o u r   a c c � d e r   �   l a   l i s t e   d e s   s y s t � m e s   d ' e x p l o i t a t i o n   e t   d e s   n a v i g a t e u r s   p r i s   e n   c h a r g e .   [�P o u r   p r o f i t e r   p l e i n e m e n t   d e s   f o n c t i o n s   d u   p r e s s e - p a p i e r   d a n s   l e   n a v i g a t e u r   F i r e f o x ,   v e u i l l e z   i n s t a l l e r   l ' e x t e n s i o n   p o u r   l e   n a v i g a t e u r . 
 P o u r   i n s t a l l e r   l ' e x t e n s i o n ,   c l i q u e z   s u r   l e   b o u t o n   " D � m a r r e r   l ' i n s t a l l a t i o n " .   L o r s q u ' u n   m e s s a g e   s ' a f f i c h e   p o u r   i n d i q u e r   q u e   F i r e f o x   a   b l o q u �   l a   d e m a n d e   d ' i n s t a l l a t i o n ,   c l i q u e z   s u r   " A u t o r i s e r " ,   p u i s   s u r   " I n s t a l l e r "   ��< p >   P o u r   e f f e c t u e r   c e t t e   a c t i o n ,   v o u s   d e v e z   i n s t a l l e r   l ' e x t e n s i o n   p o u r   l e   n a v i g a t e u r   e t   % s . < / p >   < p > P o u r   i n s t a l l e r ,   c l i q u e z   s u r   l e   b o u t o n < b > "   C o n t i n u e r " < / b > . < / p > < p > L o r s q u ' u n   m e s s a g e   s ' a f f i c h e   p o u r   i n d i q u e r   q u e   F i r e f o x   a   b l o q u �   l a   d e m a n d e   d ' i n s t a l l a t i o n ,   c l i q u e z   s u r   " A u t o r i s e r " ,   p u i s   s u r   " I n s t a l l e r " .   L o r s q u e   v o u s   � t e s   i n v i t �   �   i n s t a l l e r ,   a s s u r e z - v o u s   q u e   l ' a u t e u r   e s t   u n   f o u r n i s s e u r   d ' e x t e n s i o n   d e   n a v i g a t e u r   f i a b l e   ! < / p >   ��< p >   P o u r   e f f e c t u e r   l ' a c t i o n ,   i l   e s t   n � c e s s a i r e   d ' i n s t a l l e r   l ' e x t e n s i o n   d e   n a v i g a t e u r   e t   % s . < / p > < p > P o u r   i n s t a l l e r ,   c l i q u e z   s u r   < b > "   C o n t i n u e r " < / b > . < / p > < p > L e   p r o g r a m m e   o u v r i r a   l a   p a g e   d e   l ' e x t e n s i o n   d a n s   G o o g l e   C h r o m e   W e b   S t o r e .   C l i q u e z   s u r   l e   b o u t o n   < b > " +   & n b s p ;   G R A T U I T " < / b >   p o u r   i n s t a l l e r   l ' e x t e n s i o n .   < p >   < p > A p r � s   a v o i r   i n s t a l l �   l ' e x t e n s i o n ,   f e r m e z   l a   p a g e   d e   l ' e x t e n s i o n   e t   i n s t a l l e z   l e   c o m p o s a n t   l u i - m � m e .   < / p > < p > P o u r   c e   f a i r e ,   r e n d e z   l e   f i c h i e r   e x � c u t a b l e   e t   e x � c u t e z - l e .   < / p >   ��C l i q u e z   s u r   l e   l i e n   e n   b a s . 
 A L e   n a v i g a t e u r   t � l � c h a r g e r a   l e   f i c h i e r   d ' i n s t a l l a t i o n   c o n t e n a n t   l e   c o m p o s a n t . 
 S i   l e   f i c h i e r   n e   s ' o u v r e   p a s   a u t o m a t i q u e m e n t ,   o u v r e z - l e   v o u s - m � m e   e n   d o u b l e - c l i q u a n t   d e s s u s   d a n s   l a   f e n � t r e   d e   t � l � c h a r g e m e n t . 
 S u i v e z   l e s   i n s t r u c t i o n s   �   l ' � c r a n   p o u r   t e r m i n e r   l ' i n s t a l l a t i o n . 
 P e n d a n t   l ' i n s t a l l a t i o n ,   v o u s   p o u v e z   v � r i f i e r   l e   f o u r n i s s e u r   d u   c o m p o s a n t   e n   c l i q u a n t   s u r   l ' i c � n e   d a n s   l e   c o i n   s u p � r i e u r   d r o i t   d e   l a   f e n � t r e   d ' i n s t a l l a t i o n .   C o n t r � l e z   l e   n o m   d u   f o u r n i s s e u r .   I l   d o i t   � t r e   f i a b l e   !   �P o u r   p r o f i t e r   p l e i n e m e n t   d e s   f o n c t i o n s   d u   p r e s s e - p a p i e r   d a n s   l e   n a v i g a t e u r   C h r o m e ,   v e u i l l e z   i n s t a l l e r   l ' e x t e n s i o n   p o u r   l e   n a v i g a t e u r . 
 P o u r   i n s t a l l e r   l ' e x t e n s i o n ,   c l i q u e z   s u r   l e   b o u t o n   "   D � m a r r e r   l ' i n s t a l l a t i o n   " ,   p u i s   c l i q u e z   s u r   l e   b o u t o n   " + G r a t u i t "   s u r   l a   p a g e   o u v e r t e   ( u n   r e d � m a r r a g e   d e   l ' a p p l i c a t i o n   e s t   n � c e s s a i r e ) . 
 P o u r   i n s t a l l e r   a u t o m a t i q u e m e n t   l ' e x t e n s i o n   l a   p r o c h a i n e   f o i s   q u e   v o u s   d � m a r r e z   l ' a p p l i c a t i o n ,   c l i q u e z   s u r   l e   b o u t o n   " S u s p e n d r e   l ' i n s t a l l a t i o n " . 
 C l i q u e z   s u r   " A n n u l e r "   p o u r   n e   p a s   i n s t a l l e r   l ' e x t e n s i o n   m a i n t e n a n t .   �< p >   P o u r   c e t t e   a c t i o n ,   v o u s   d e v e z   i n s t a l l e r   l ' e x t e n s i o n   d e   n a v i g a t e u r   e t   % s . < / p > < p > P o u r   c o m m e n c e r   l ' i n s t a l l a t i o n ,   c l i q u e z   s u r   < b > " C o n t i n u e r " < / b > . < / p > < p > L e   p r o g r a m m e   o u v r i r a   l a   p a g e   d e   l ' e x t e n s i o n   d a n s   G o o g l e   C h r o m e   W e b   S t o r e .   C l i q u e z   s u r   l e   b o u t o n   < b > " + & n b s p ; G R A T U I T " < / b >   p o u r   i n s t a l l e r   l ' e x t e n s i o n .   < / p > < p > A p r � s   a v o i r   i n s t a l l �   l ' e x t e n s i o n ,   f e r m e z   l a   p a g e   d e   l ' e x t e n s i o n   e t   i n s t a l l e z   l e   c o m p o s a n t   l u i - m � m e . < / p > < p >   P o u r   c e   f a i r e ,   c l i q u e z   s u r   l e   f i c h i e r   o b t e n u   ( e n   b a s   d e   l a   f e n � t r e   d u   n a v i g a t e u r )   e t   a t t e n d e z   l a   f i n   d e   l ' i n s t a l l a t i o n . < / p >   �������������
  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Comment configurer votre navigateur</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar>
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.9600.16476"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Configuration du navigateur pour 1C: Entreprise</P>
<P class="title step">Pas 1</P>
<P>Cliquez sur l'icône <B><I>Outils</I></B>, qui est situé dans le coin supérieur droit de la fenêtre.</P>
<P>Dans le menu, choisissez <B><I>Paramètres</I></B>.</P>
<DIV><IMG border=0 src="img_ch_1_en.png?sysver=<%V8VER%>"></DIV>
<P>Onglet <B><I>Paramètres principaux</I></B>s'ouvre. Toutes les actions suivantes se passent sur cette page.</P>
<P>Pour revenir à l'instruction choisissez l'onglet <B><I>Comment configurer le navigateur</I></B>.</P>
<DIV><IMG border=0 src="img_ch_0_en.png?sysver=<%V8VER%>"></DIV>
<P class="title step">Pas 2</P>
<P>Naviguez au lien <B><I>Afficher les paramètres avancés...</I></B> au bas de l'écran.</P>
<DIV><IMG border=0 src="img_ch_2_en.png?sysver=<%V8VER%>"></DIV>
<P>Aller au bloc <B><I>Téléchargements</I></B> et cochez la case <B><I>Demander où enregistrer chaque fichier avant de le téléchargement</I></B>.</P>
<DIV><IMG border=0 src="img_ch_3_en.png?sysver=<%V8VER%>"></DIV>
<P class="title step">Pas 3</P>
<P>Après avoir marquer le crochet, appuyez sur <B><I>Paramètres de contenu</I></B>, qui est situé au-dessus au niveau du bloc <B><I>Données personnelles</I></B>.</P>
<DIV><IMG border=0 src="img_ch_4_en.png?sysver=<%V8VER%>"></DIV>
<P>Dans la fenêtre qui apparaît <B><I>Paramètres de contenu</I></B> naviguez au bloc <B><I>Pop-Ups</I></B> et marquer <B><I>Autoriser tous les sites à afficher des pop-ups</I></B>.</P>
<DIV><IMG border=0 src="img_ch_5_en.png?sysver=<%V8VER%>"></DIV>
<P style="BACKGROUND-COLOR: #e4e4e4">S'il vous plaît noter que votre navigateur n'est pas nécessaire d'enregistrer les modifications. Pour terminer le réglage, il suffit de fermer l'onglet <I>Paramètres</I>.</P></DIV></DIV></BODY></HTML>��������������
  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Comment configurer votre navigateur</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Configuration du navigateur pour 1C: Entreprise</p>
<p class="title step">Pas 1</p>

<p>Cliquez sur l'icône <b><i>Outils</i></b>, qui est situé dans le coin supérieur droit de la fenêtre.</p>
<p>Dans le menu, choisissez <b><i>Paramètres</i></b>.</p>

<div><img border=0 src="img_ch_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Onglet <b><i>Paramètres principaux</i></b> s'ouvre. Toutes les actions suivantes se passent sur cette page.</p> 
<p>Pour revenir à l'instruction choisissez l'onglet <b><i>Comment configurer le navigateur</i></b>.</p>

<div><img border=0 src="img_ch_0_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Pas 2</p>

<p>Naviguez au lien <b><i>Afficher les paramètres avancés</i></b> au bas de l'écran.</p>

<div><img border=0 src="img_ch_2_ru.png?sysver=<%V8VER%>" /></div>

<p>Aller au bloc <b><i>Téléchargements</i></b> et cochez la case <b><i>Demander où enregistrer chaque fichier avant de le téléchargement</i></b>.</p>

<div><img border=0 src="img_ch_3_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Pas 3</p>

<p>Après avoir marquer le crochet, appuyez sur <b><i>Paramètres de contenu</i></b>, qui est situé au-dessus au niveau du bloc <b><i>Données personnelles</i></b>.</p>

<div><img border=0 src="img_ch_4_ru.png?sysver=<%V8VER%>" /></div>

<p>Dans la fenêtre qui apparaît <b><i>Paramètres de contenu</i></b> naviguez au bloc <b><i>Pop-Ups</i></b> et marquer <b><i>Autoriser tous les sites à afficher des pop-ups</i></b>.</p>

<div><img border=0 src="img_ch_5_ru.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">S'il vous plaît noter que votre navigateur n'est pas nécessaire d'enregistrer les modifications. Pour terminer le réglage, il suffit de fermer l'onglet <i>Paramètres</i>.</p>

</div>
</div>

</body>
</html>
������������	  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Comment configurer le navigateur</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar>
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.10240.16566"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Configuration du navigateur pour 1C:Enterprise</P>
<UL>
<LI>Pour autoriser les fenêtres pop-up dans le menu de l'application, choisissez l'option <B>Paramètres (Options)</B> dans le menu  <B>Outils (Tools)</B> du navigateur.;<BR>Dans la nouvelle fenêtre cliquez sur la section <B>Contenu (Content)</B>;<BR>Décochez <B>Bloquer des fenêtres pop-up (Block pop-up windows)</B>.
<LI>Pour autoriser manuellement le changement des fenêtres de l'applications entrez <B>about:config</B> dans la barre d'adresse du navigateur;<BR>Ensuite tapez <CODE>dom.disable_window_flip</CODE> dans la chaine du filtre;<BR>Changer la valeur de cette option à <CODE>faux</CODE>. 
<LI>Pour autoriser manuellement l'utilisation du clavier pour le changement des fenêtres de l'applications entrez <B>about:config</B>dans la barre d'adresse du navigateur;<BR>Ensuite tapez <CODE>dom.popup_allowed_events</CODE> dans la chaine du filtre;<BR>Ajouter à la valeur de cette option l'évènement <B>keydown</B>
<LI>Pour configurer manuellement l'authentification du système d'exploitation, tapez <B>about:config</B> dans la barre d'adresse du navigateur;<BR> Ensuite tapez le nom du paramètre dans la chaîne du filtre;<BR>Cette option contient 3 paramètres: <CODE>network.automatic-ntlm-auth.trusted-uris</CODE>, <CODE>network.negotiate-auth.delegation-uris</CODE>, <CODE>network.negotiate-auth.trusted-uris</CODE>;<BR>Ensuite, spécifiez la liste de serveurs Web àpour "1C:Enterprise".</LI></UL>
<P>La configuration est terminée.</P></DIV></DIV></BODY></HTML>��������d	  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Comment configurer votre navigateur</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Configuration du navigateur pour 1C: Entreprise</p>
<p class="title step">Шаг 1</p>

<p>Cliquez avec le bouton droit de la souris sur une zone vide dans la barre d'adresse (photo marquée rouge pâle) 
et sélectionnez <b><i>Menu Bar</i></b>.</p>

<div><img border=0 src="img_ie_0_en.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="img_ie_01_en.png?sysver=<%V8VER%>" /></div>

<p>Le menu s'apparaît sous la barre d'adresse. Trouvez l'item <b><i>Outils</i></b>.</p>

<div><img border=0 src="img_ie_1_en.png?sysver=<%V8VER%>" /></div>

<p>Cliquez sur lui. Le menu s'affiche. Sélectionner <b><i>Paramètres d'internet</i></b>.</p>

<div><img border=0 src="img_ie_2_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Pas 2</p>

<p>Dans la fenêtre qui apparaît, cliquez sur l'onglet <b><i>Données personnelles</i></b>.</p>

<div><img border=0 src="img_ie_3_en.png?sysver=<%V8VER%>" /></div>

<p>Trouvez et <b><i>décochez</i></b> le crochet <b><i>Désactiver les pop-ups</i></b>.</p>

<div><img border=0 src="img_ie_4_en.png?sysver=<%V8VER%>" /></div>

<p>Une fois le crochet décoché, cliquez sur <b><i>ОК</i></b>, pour enregistrer les paramètres.</p>

<div><img border=0 src="img_ie_5_en.png?sysver=<%V8VER%>" /></div>

<p>La configuration est terminée.</p>

<p style='font-size:9pt'>Pour masquer le menu au-dessous de la barre d'adresse, répéter la première partie de l'étape 1.</p>

</div>
</div>

</body>
</html>
��������d	  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Comment configurer votre navigateur</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Configuration du navigateur pour 1C: Entreprise</p>
<p class="title step">Шаг 1</p>

<p>Cliquez avec le bouton droit de la souris sur une zone vide dans la barre d'adresse (photo marquée rouge pâle) 
et sélectionnez <b><i>Menu Bar</i></b>.</p>

<div><img border=0 src="img_ie_0_ru.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="img_ie_01_ru.png?sysver=<%V8VER%>" /></div>

<p>Le menu s'apparaît sous la barre d'adresse. Trouvez l'item <b><i>Outils</i></b>.</p>

<div><img border=0 src="img_ie_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Cliquez sur lui. Le menu s'affiche. Sélectionner <b><i>Paramètres d'internet</i></b>.</p>

<div><img border=0 src="img_ie_2_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Pas 2</p>

<p>Dans la fenêtre qui apparaît, cliquez sur l'onglet <b><i>Données personnelles</i></b>.</p>

<div><img border=0 src="img_ie_3_ru.png?sysver=<%V8VER%>" /></div>

<p>Trouvez et <b><i>décochez</i></b> le crochet <b><i>Désactiver les pop-ups</i></b>.</p>

<div><img border=0 src="img_ie_4_ru.png?sysver=<%V8VER%>" /></div>

<p>Une fois le crochet décoché, cliquez sur <b><i>ОК</i></b>, pour enregistrer les paramètres.</p>

<div><img border=0 src="img_ie_5_ru.png?sysver=<%V8VER%>" /></div>

<p>La configuration est terminée.</p>

<p style='font-size:9pt'>Pour masquer le menu au-dessous de la barre d'adresse, répéter la première partie de l'étape 1.</p>

</div>
</div>

</body>
</html>
���������	  ﻿<!DOCTYPE html>
<html class="IWeb" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Browser setup</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="imagetoolbar" content="no">
        <link href="<%CSS_FILE%>?sysver=<%V8VER%>" rel="stylesheet" type="text/css">
        <style type="text/css">
        .main
        {
            width:720px;
            margin:0 auto;
        }
        .head
        {
            font-size:32pt;
            font-family:Times New Roman;
        }
        .title
        {
            font-size:20pt;
            font-family:Times New Roman;
        }
        .txt
        {
            font-size:14pt;
            font-family:Times New Roman;
        }
        </style>
</head>
<body style="width:100%;height:100%;overflow:hidden;padding:0;margin:0;">
<div style="width:100%;height:100%;overflow-y:auto;overflow-x:hidden;">
<div style="padding:10px;padding-left:30px;padding-right:30px;">
<p><span class="txt"><b>A new window has been blocked, presumably by a pop-up blocker.</b></span></p>
<p><span class="txt">To continue, configure your web browser.<br> To view the instruction, click <b><i>View instruction</i></b>.<br> When you are done, click <b><i>OK</i></b> and restart the application.</span></p>
<div align="center" style="width:100%;height:50px;padding-top:20px;">
    <div style="width:450px;height:30px;position:relative;">
    <button id="okButton" class="webButton" style="left:10px;">View instruction</button>
    <button id="cancelButton" class="webButton" style="left:230px;">OK</button>
    </div>
    </div>
</div>
</div>
<!--@remove+@-->
<script type="text/javascript">
    var CLOSURE_NO_DEPS = true;
    var CLOSURE_BASE_PATH = "scripts/";
</script>
<script type="text/javascript" src="scripts/webtools/libs/closure-library/closure/goog/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/deps.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/dbgstart_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
<!--@remove-@-->
<script type="text/javascript" src="scripts/mod_browsersettingsinfomain_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
</body>
</html>
���������
  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Comment configurer votre navigateur</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Configuration du navigateur pour 1C: Entreprise</p>

<p>Dans le coin supérieur gauche trouver et cliquez sur l'élément <b><i>Safari</i></b>. Décochez <b><i>Désactiver les pop-ups</i></b> dans le menu.</p>

<div><img border=0 src="img_sf_en.png?sysver=<%V8VER%>" /></div>

<p>La configuration est terminée.</p>

</div>
</div>

</body>
</html>
��
  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Comment configurer votre navigateur</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Configuration du navigateur pour 1C: Entreprise</p>

<p>Dans le coin supérieur gauche trouver et cliquez sur l'élément <b><i>Safari</i></b>. Décochez <b><i>Désactiver les pop-ups</i></b> dans le menu.</p>

<div><img border=0 src="img_sf_ru.png?sysver=<%V8VER%>" /></div>

<p>La configuration est terminée.</p>

</div>
</div>

</body>
</html>
��� ��� �#��<�\5�X�<���u�����o3t���^F � � _ � z � ��d?_E2��
���N	�D��~�G`�Y5
V
���r
�	�
z
�0	�$
�
v	��v���	�	0I�	�d
	Z�����	�
��	��
���
�9Ll%E��  ]�wj$Y=��w��(Gj���*A}`�C&���4 `  `  ` `L `Q `� `� `L `S `� `E `s `� `E ` `� `� `� `�& `�  `R `�$ `� `� `d `� `� `n `j  `�	 `� `� `�
 `� `� ` `� `O  `0 `� `� `c `~  `�	 `� `� ` ` `� `� `U	 `� `t `R `� `t  `2 `  `�  `� `G
 ` `  `/  ` `| ` `� ``  `� `� `B `
 `� `� `y `'  `�  `G  `G `� `m `� `  `�  ` `�  `�  `� `2 `q ` `# `3 `5 `\ `�  `  `� `� `7  `?  `G `' `� `Y `W  `v `R `� ` `� `$ `� `�  `= `E `	 ` `	 ` `@ ` `� `�" `�  ` `� `�  `�
 `� `3 `�  `� `� `� `� `� `P `k `� `P `� `) `c `v `� `� `� K  o  �" '% �' �( 